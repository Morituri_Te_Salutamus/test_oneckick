from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
import time
from selenium.webdriver.common.keys import Keys

# Использовать time.sleep() - не правильно, я понимаю. EXPLICIT WAIT в процессе изучения.


class TestClass:
    # Открываем сайт
    def test_open_site(self, driver):
        driver.get("https://oneclickmoney.ru")
        assert driver.title == "Онлайн займы на карту, оформление микрозаймов и экспресс-кредитов от Oneclickmoney"

    # Кликаем на кнопку "Как это работает" и сверяем текст открытого окна
    def test_button(self, driver):
        tabs = driver.find_element_by_css_selector('.inner-tabs-ctc.tabs-global')
        tabs.find_element_by_css_selector('button:last-child').click()
        time.sleep(2)
        get_text = driver.find_element_by_css_selector('.title-howiw').text
        assert get_text == 'Cумма и срок займа'

    # Кликаем на кнопку "Взять займ" и сверяем текст открытого окна
    def test_button_2(self, driver):
        tabs = driver.find_element_by_css_selector('.inner-tabs-ctc.tabs-global')
        tabs.find_element_by_css_selector('button:first-child').click()
        time.sleep(2)
        text_in_window = driver.find_element_by_css_selector('.pv-active.range-blk-ctc.data-range-am1 p').text
        assert text_in_window == 'Сумма займа'

    # Переходим в окно регистрации
    def test_reg(self, driver):
        icon = driver.find_element_by_css_selector('.reg-head-elm')
        icon.click()
        icon.find_element_by_css_selector('div.regestration-link').click()
        assert driver.title == r'Быстрая регистрация на сервисе OneClickMoney для получения онлайн-займа на ' \
                               r'дебетовую карту, через платежную систему, QIWI и другими способами'

    # Заполняем регистрационную форму и переходим на главную страницу
    def test_regform_input(self, driver):
        mail_form = driver.find_element_by_css_selector('#usermodel-email')
        mail_form.click()
        mail_form.send_keys("test@test.com")

        phone_form = driver.find_element_by_css_selector('#usermodel-mobile_phone')
        phone_form.click()
        phone_form.send_keys("9999999999")

        last_name = driver.find_element_by_css_selector("#usermodel-last_name")
        last_name.click()
        last_name.send_keys("Пупкин")

        first_name = driver.find_element_by_css_selector('#usermodel-first_name')
        first_name.click()
        first_name.send_keys("Владимир")

        middle_name = driver.find_element_by_css_selector('#usermodel-middle_name')
        middle_name.click()
        middle_name.send_keys("Владимирович")

        password = driver.find_element_by_css_selector('#usermodel-password')
        password.click()
        password.send_keys("1234567890qwe")

        password_confirm = driver.find_element_by_css_selector('#usermodel-confirm_password')
        password_confirm.click()
        password_confirm.send_keys("1234567890qwe")

        submit_button = driver.find_element_by_css_selector("div.sub-wr-frm-reg a")
        submit_button.click()

        driver.find_element_by_css_selector('div.modal-close').click()
        time.sleep(2)
        logo = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'div.logo-sclled')))
        logo.click()

    # Двигаем слайдер
    def test_slider(self, driver):
        slider = driver.find_element_by_css_selector('.rangeslider__handle')
        move = ActionChains(driver)
        x = 0
        while x < 200:
            move.click_and_hold(slider).move_by_offset(x, 0).release().perform()
            x += 80

    # Переключаемся между отзывами
    def test_feed(self, driver):
        driver.execute_script("arguments[0].scrollIntoView();",
                              driver.find_element_by_css_selector('.title-clients-com-m'))
        feed_arrow = driver.find_element_by_css_selector('.arrow-right-feed')
        x = 0
        while x < 5:
            feed_arrow.click()
            time.sleep(1)
            x += 1

    # Расскрываем подробно FAQ
    def test_faq(self, driver):
        faq = driver.find_element_by_css_selector('div.namecomm-feed')
        driver.execute_script("arguments[0].scrollIntoView();", faq)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'div.question-m-blk')))
        faq_collumn = driver.find_elements_by_css_selector('div.question-m-blk')
        for elem in range(len(faq_collumn)):
            faq_collumn[elem].click()
            time.sleep(1)
            faq_collumn[elem].click()

    # Проход в О компании
    def test_link_about(self, driver):
        driver.find_element_by_tag_name('body').send_keys(Keys.END)
        about = driver.find_element_by_css_selector(
            "div.top-con-footer.clearfix div.list-info-f:nth-of-type(2) ul li:nth-of-type(1) a")
        about.click()
        time.sleep(3)

    # Проход по ссылкам
    def test_link(self, driver):
        tab = driver.find_elements_by_css_selector('div.inner-about-tabs.tabs-global a')
        for i in range(len(tab)):
            tab = driver.find_elements_by_css_selector('div.inner-about-tabs.tabs-global a')
            tab[i].click()
            time.sleep(2)
