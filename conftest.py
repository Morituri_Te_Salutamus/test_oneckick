import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

@pytest.fixture(scope="class")
def driver(request):
    drv = webdriver.Chrome(ChromeDriverManager().install())
    drv.implicitly_wait(10)
    drv.maximize_window()
    yield drv
    drv.close()

